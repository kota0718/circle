class HomeController < ApplicationController

  def top
    if user_signed_in?
      @event = Event.new
      @events = Event.all.order(created_at: :desc)
    else
      @message = "Welcome to Circle!"
    end
  end

  def about
  end
end
