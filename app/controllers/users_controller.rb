class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: [:edit, :update]

  before_action :set_user, only: [:show, :edit, :update]


  def index
    @users = User.all
  end

  def show
    @events = @user.events
  end

  def edit
  end

  def update
    file = params[:user][:image]
    @user.set_image(file)

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email)
    end

    def correct_user
      user = User.find(params[:id])

      if current_user.id != user.id
        redirect_to root_path, alert: "The page you've tried to visit is not allowed"
      end
    end
end
