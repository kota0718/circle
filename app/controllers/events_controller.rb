class EventsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: [:edit, :update]

  before_action :set_event, only: [:show, :edit, :update, :destroy]

  def create
    @event = current_user.events.build(event_params)
    if @event.save
      redirect_to @event, notice: "Successfully created"
    else
      @events = Event.all.order(created_at: :desc)
      render 'home/top'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @event.update(event_params)
      redirect_to @event, notice: "success"
    else
      render :edit
    end
  end

  def destroy
    @event.destroy
    redirect_to events_path
  end

  private

    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:title, :description)
    end

    def correct_user
      event = Event.find(params[:id])

      if !current_user?(event.user)
        redirect_to root_path, alert: "The page you've tried to visit is not allowed"
      end
    end

end
