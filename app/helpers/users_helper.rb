module UsersHelper


  def image_for(user)
    # user = current_user
    if user.image
      image_tag "/user_images/#{user.image}", class: "profile_img"
    else
      image_tag "default_profile.png", class: "profile_img"
    end
  end

end
