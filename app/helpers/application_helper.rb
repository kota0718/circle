module ApplicationHelper

  def current_user?(user)
    current_user.id == user.id
  end

  def check_image?(user)
    user.id
  end

end
