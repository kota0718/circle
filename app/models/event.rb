class Event < ApplicationRecord
  belongs_to :user

  validates :title, presence: true
  validates :description, presence: true, length: {maximum: 140}
  validates :user_id, presence: true
end
